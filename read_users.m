% ________________________________________________________________________
%   read_users(path)
%   Function parses XML containing user information, which path was passed
%   in argument and returns array of user structures.
%_________________________________________________________________________

function users = read_users(path)
% parse XML and create DOM model
try
   xdoc = xmlread(path);
catch e
    disp(e);
    error('Failed to read user data %s.', path);
end

% get list of all users
userNodes = xdoc.getElementsByTagName('user');
% preallocating memory for users TODO better
users(userNodes.getLength) = create_user('', '', '');
    for k = 0 : userNodes.getLength - 1
       % take one user from list
        currentUserNode = userNodes.item(k);
        % get all users data
        login = getElement(currentUserNode, 'login');
        password = getElement(currentUserNode, 'password');
        file = getElement(currentUserNode, 'file');
        X = sprintf('login: %s, pass: %s, file: %s.', login, password, file);
        disp(X);
        
        % getting dataset node
        datasetNodes = currentUserNode.getElementsByTagName('datasets');
        % iterating all the dataset nodes
        %data1 = getElement(datasetNodes, 'dataset');
        
        disp(datasetNodes.length);
        
        % create struct user and add it to array
        users(k+1) = create_user(login, password, file);
    end

end
% ________________________________________________________________________
%   creat_user(login, password, file)
%   Function creates structure containing data passed in arguments.
% ________________________________________________________________________     
function user = create_user(login, password, file)
    user.login = login;
    user.password = password;
    user.file = file;
end
% ________________________________________________________________________
%   getElement(currentNode, tagString)
%   Function get instance currentNode and tag name from tagString
%   It returns string from one-element xml file
% ________________________________________________________________________     
function retTag = getElement(currentNode, tagString)
    retTag = currentNode.getElementsByTagName(tagString).item(0).getFirstChild.getData;
    retTag = char(retTag);
end
