function eeg_auth()
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GUI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % creating main window
    f = figure('Name', 'EEG Auth DEMO', 'Position', [300 300 380 260]);
    % creating send button
    send_bt = uicontrol(f, 'Style', 'pushbutton', 'String', 'Submit', 'Position', [270 100 70 35]);
    % setting callback function to send bt
    set(send_bt, 'Callback', @send_callback);
    % creating static texts
    uicontrol(f, 'Style', 'text', 'String', 'Enter login:', 'Position', [30 220 90 20]);
    uicontrol(f, 'Style', 'text', 'String', 'Enter password:', 'Position', [30 170 90 20]);
    % creating text edits for inputs
    login_e = uicontrol(f, 'Style', 'edit', 'Position', [190 220 150 20]);
    password_e = uicontrol(f, 'Style', 'edit', 'Position', [190 170 150 20]);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF GUI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%% 'GLOBAL' VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % read all registered users (stored in array)
    users = read_users('users.xml');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % ____________________________________________________________________
    % login_user()
    % Function reads the entered inputs from GUI's text edits a try to login 
    % user. If entered password and login belong to registered user
    % ('users.xml') then proceeds with biometric test.
    % ____________________________________________________________________
    function login_user()
        login = get(login_e, 'String');
        password = get(password_e, 'String');
        
        if ~check_user(login, password)
            % kdyz to neni user
            disp('spatne udaje')
        else
            % kdyz to je user
            disp('spravne udaje')
        end
        
    end
    
    % ____________________________________________________________________ 
    % check_user()
    % Return 1 if the entered login and password are 'right' else return 0.
    % ____________________________________________________________________
    function isUser = check_user(login, password)
       for i = 1 : length(users)
            if strcmp(users(i).login, login)&&strcmp(users(i).password, password)
            isUser = 1;
            return
            end
       end
       isUser = 0;
    end
    
    % ____________________________________________________________________
    %  send_callback(h, eventdata)
    %  Callback function which is called after pressing the 'send button'.
    % ____________________________________________________________________
    
    function send_callback(h, eventdata)
        login_user();
    end
end